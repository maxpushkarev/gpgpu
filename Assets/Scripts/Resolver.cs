﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class Resolver : MonoBehaviour
{
	private const int GENERATION_COUNT = 64;
	private const int MAX_ITERATION_COUNT = 1000;
	private const int MIN_ERROR = 0;
	private const string ITERATION_NUMBER = "iterationNumber";
	private const string SORT_ITERATION_I = "sortIterationI";
	private const string SORT_ITERATION_J = "sortIterationJ";
	private const string SEED_DEFORMATION = "seedDeformation";
	
	private const string A_NAME = "A";
	private const string B_NAME = "B";
	private const string C_NAME = "C";
	private const string D_NAME = "D";
	private const string F_NAME = "F";

	private const int MAX_THREADS_COUNT = 1024;
	private const int INCIDENTS_COUNT = GENERATION_COUNT * GENERATION_COUNT;

	[SerializeField]
	private Button btn;
	[SerializeField]
	private Text outputText;

	[SerializeField]
	private InputField xInputField;
	[SerializeField]
	private InputField yInputField;
	[SerializeField]
	private InputField zInputField;
	[SerializeField]
	private InputField wInputField;
	[SerializeField]
	private InputField fInputField;

	[SerializeField]
	private ComputeShader newGenerationShader;
	[SerializeField]
	private ComputeShader firstGenerationShader;
	[SerializeField]
	private ComputeShader searchCandidateShader;

	private static int a, b, c, d, f;
	private float x, y, z, w;

	private int firstGenerationKernel;
	private int searchCandidateKernel;
	private int getSolutionKernel;
	private int fillSolutionsKernel;

	private int clearIncidentsKernel;
	private int copyIncidentsKernel;
	private int aggregateIncidentsKernel;
	private int sumIncidentsKernel;
	private int selectKernel;
	private int createParentIndexBufferKernel;
	private int writeGenerationKernel;
	private int sortParentIndexBufferKernel;

	private ComputeBuffer generationBuffer;
	private ComputeBuffer solutionBuffer;
	private ComputeBuffer singleSolutionBuffer;

	private ComputeBuffer accumulationBuffer;
	private ComputeBuffer singleAccumulatedBuffer;

	private ComputeBuffer parentIndexBuffer;
	private ComputeBuffer incidentsBuffer;
	private ComputeBuffer accumulationIncidentsBuffer;
	private ComputeBuffer sumIncidentsBuffer;
	private int incidentsGroupCount;

	private int log2IncidentsCount;
	private int log2GenerationCount;
	private ResolvingVariant[] solutions;
	private float[] pairSum;

	private void Awake()
	{
		xInputField.text = "1";
		yInputField.text = "2";
		zInputField.text = "3";
		wInputField.text = "4";
		fInputField.text = "50";

		pairSum = new float[1];
		log2GenerationCount = Mathf.FloorToInt(Mathf.Log(GENERATION_COUNT, 2));
		solutions = new ResolvingVariant[1];

		firstGenerationKernel = firstGenerationShader.FindKernel("CreateFirstGeneration");
		searchCandidateKernel = searchCandidateShader.FindKernel("SearchCandidate");
		fillSolutionsKernel = searchCandidateShader.FindKernel("FillSolutions");
		getSolutionKernel = searchCandidateShader.FindKernel("GetSingleSolution");

		clearIncidentsKernel = newGenerationShader.FindKernel("ClearIncidents");
		copyIncidentsKernel = newGenerationShader.FindKernel("CopyIncidents");
		aggregateIncidentsKernel = newGenerationShader.FindKernel("AggregateIncidents");
		sumIncidentsKernel = newGenerationShader.FindKernel("SumIncidents");
		selectKernel = newGenerationShader.FindKernel("Select");
		createParentIndexBufferKernel = newGenerationShader.FindKernel("CreateParentIndexBuffer");
		writeGenerationKernel = newGenerationShader.FindKernel("WriteGeneration");
		sortParentIndexBufferKernel = newGenerationShader.FindKernel("SortParentIndexBuffer");

		generationBuffer = new ComputeBuffer(GENERATION_COUNT, 20);
		solutionBuffer = new ComputeBuffer(GENERATION_COUNT, 20);
		singleSolutionBuffer = new ComputeBuffer(1, 20);

		accumulationBuffer = new ComputeBuffer(GENERATION_COUNT, 4);
		singleAccumulatedBuffer = new ComputeBuffer(1, 4);

		parentIndexBuffer = new ComputeBuffer(INCIDENTS_COUNT, 12);
		log2IncidentsCount = Mathf.FloorToInt(Mathf.Log(INCIDENTS_COUNT, 2));
		incidentsBuffer = new ComputeBuffer(INCIDENTS_COUNT, 4);
		accumulationIncidentsBuffer = new ComputeBuffer(INCIDENTS_COUNT, 4);
		sumIncidentsBuffer = new ComputeBuffer(1, 4);
		incidentsGroupCount = INCIDENTS_COUNT / MAX_THREADS_COUNT;

		firstGenerationShader.SetBuffer(firstGenerationKernel, "generation", generationBuffer);

		searchCandidateShader.SetBuffer(searchCandidateKernel, "accumulationBuffer", accumulationBuffer);
		searchCandidateShader.SetBuffer(searchCandidateKernel, "solution", solutionBuffer);

		searchCandidateShader.SetBuffer(fillSolutionsKernel, "generation", generationBuffer);
		searchCandidateShader.SetBuffer(fillSolutionsKernel, "solution", solutionBuffer);
		searchCandidateShader.SetBuffer(fillSolutionsKernel, "accumulationBuffer", accumulationBuffer);

		searchCandidateShader.SetBuffer(getSolutionKernel, "singleSolution", singleSolutionBuffer);
		searchCandidateShader.SetBuffer(getSolutionKernel, "solution", solutionBuffer);
		searchCandidateShader.SetBuffer(getSolutionKernel, "accumulationBuffer", accumulationBuffer);
		searchCandidateShader.SetBuffer(getSolutionKernel, "singleAccumulatedBuffer", singleAccumulatedBuffer);

		newGenerationShader.SetBuffer(clearIncidentsKernel, "incidentsBuffer", incidentsBuffer);
		newGenerationShader.SetBuffer(aggregateIncidentsKernel, "accumulationIncidentsBuffer", accumulationIncidentsBuffer);
		newGenerationShader.SetBuffer(copyIncidentsKernel, "accumulationIncidentsBuffer", accumulationIncidentsBuffer);
		newGenerationShader.SetBuffer(copyIncidentsKernel, "incidentsBuffer", incidentsBuffer);
		newGenerationShader.SetBuffer(sumIncidentsKernel, "accumulationIncidentsBuffer", accumulationIncidentsBuffer);
		newGenerationShader.SetBuffer(sumIncidentsKernel, "sumIncidentsBuffer", sumIncidentsBuffer);
		newGenerationShader.SetBuffer(selectKernel, "solution", solutionBuffer);
		newGenerationShader.SetBuffer(selectKernel, "singleAccumulatedBuffer", singleAccumulatedBuffer);
		newGenerationShader.SetBuffer(selectKernel, "incidentsBuffer", incidentsBuffer);
		newGenerationShader.SetBuffer(createParentIndexBufferKernel, "incidentsBuffer", incidentsBuffer);
		newGenerationShader.SetBuffer(createParentIndexBufferKernel, "parentIndexBuffer", parentIndexBuffer);
		newGenerationShader.SetBuffer(writeGenerationKernel, "parentIndexBuffer", parentIndexBuffer);
		newGenerationShader.SetBuffer(writeGenerationKernel, "solution", solutionBuffer);
		newGenerationShader.SetBuffer(writeGenerationKernel, "generation", generationBuffer);
		newGenerationShader.SetBuffer(sortParentIndexBufferKernel, "parentIndexBuffer", parentIndexBuffer);
	}

	private void OnApplicationQuit()
	{
		generationBuffer.Release();
		solutionBuffer.Release();
		singleSolutionBuffer.Release();
		accumulationBuffer.Release();
		singleAccumulatedBuffer.Release();
		incidentsBuffer.Release();
		accumulationIncidentsBuffer.Release();
		sumIncidentsBuffer.Release();
		parentIndexBuffer.Release();
	}

	public void Resolve()
	{
		btn.interactable = false;
		outputText.gameObject.SetActive(false);

		if (!ParseInputCoeffs())
		{
			outputText.text = "Invalid input coeffs";
		}
		else
		{
			WriteCoeffs(firstGenerationShader);
			WriteCoeffs(newGenerationShader);

			if (ExecuteResolving ()) {
				outputText.text = string.Format("Result: [x={0} ; y={1} ; z={2}; w={3}]", x,y,z,w);
			} else {
				outputText.text = "No solution found";
			}
		}

		outputText.gameObject.SetActive(true);
		btn.interactable = true;
	}

	private void WriteCoeffs(ComputeShader shader)
	{
		shader.SetFloat(A_NAME, a);
		shader.SetFloat(B_NAME, b);
		shader.SetFloat(C_NAME, c);
		shader.SetFloat(D_NAME, d);
		shader.SetFloat(F_NAME, f);
	}

	private bool ExecuteResolving()
	{
		int i = 0;
		ResolvingVariant solution = new ResolvingVariant(0,0,0,0);

		CreateFirstGeneration(out solution);

		while (true)
		{
			if (solution.error <= MIN_ERROR)
			{
				x = solution.x;
				y = solution.y;
				z = solution.z;
				w = solution.w;

				Debug.Log("Iterations = "+i);
				Debug.Log(solution);
				return true;
			}

			if (i == MAX_ITERATION_COUNT)
			{
				return false;
			}

			CreateNewGeneration(ref solution);
			i++;
		}
	}


	private void CreateNewGeneration(ref ResolvingVariant solution) {
		ExecuteSelection();
		ExecuteMerge();
		FindSolution(out solution);
	}


	private void ExecuteMerge()
	{
		newGenerationShader.Dispatch(createParentIndexBufferKernel, incidentsGroupCount, 1, 1);
		SortParentIndexBuffer();
		WriteGeneration();
	}

	private void SortParentIndexBuffer()
	{
		for (int i = 0; i < log2IncidentsCount; i++)
		{
			for (int j = 0; j <= i; j++)
			{
				newGenerationShader.SetInt(SORT_ITERATION_I, i);
				newGenerationShader.SetInt(SORT_ITERATION_J, j);
				newGenerationShader.Dispatch(sortParentIndexBufferKernel, incidentsGroupCount, 1, 1);
			}
		}
	}

	private void WriteGeneration()
	{
		MakeSeedDeformation(newGenerationShader);
		newGenerationShader.Dispatch(writeGenerationKernel, 1, 1, 1);
	}

	private void ExecuteSelection()
	{
		SavePreviousGeneration();
		ClearIncidenceMatrix();

		int pairCount = 0;
		do
		{
			MakeSeedDeformation(newGenerationShader);
			newGenerationShader.Dispatch(selectKernel, 1, 1, 1);
			pairCount = GetPairCount();
		}
		while ((pairCount < GENERATION_COUNT));
	}

	private void SavePreviousGeneration()
	{
		searchCandidateShader.Dispatch(fillSolutionsKernel, 1, 1, 1);
	}

	private void ClearIncidenceMatrix()
	{
		newGenerationShader.Dispatch(clearIncidentsKernel, incidentsGroupCount, 1, 1);
	}

	private int GetPairCount()
	{
		newGenerationShader.Dispatch(copyIncidentsKernel, incidentsGroupCount, 1, 1);
		
		for (int i = 0; i < log2IncidentsCount; i++)
		{
			newGenerationShader.SetInt(ITERATION_NUMBER, i);
			newGenerationShader.Dispatch(aggregateIncidentsKernel, incidentsGroupCount, 1, 1);
		}

		newGenerationShader.Dispatch(sumIncidentsKernel, 1, 1, 1);
		sumIncidentsBuffer.GetData(pairSum);
		return Mathf.FloorToInt(pairSum[0]);
	}
	


	private void CreateFirstGeneration(out ResolvingVariant solution)
	{
		CreateFirstGeneration();
		FindSolution(out solution);
	}


	private void CreateFirstGeneration()
	{
		MakeSeedDeformation(firstGenerationShader);
		firstGenerationShader.Dispatch(firstGenerationKernel, 1, 1, 1);
	}

	private void MakeSeedDeformation(ComputeShader shader)
	{
		shader.SetInt(SEED_DEFORMATION, UnityEngine.Random.Range((int)UInt32.MinValue, Int32.MaxValue));
	}

	private void FindSolution(out ResolvingVariant solution)
	{
		searchCandidateShader.Dispatch(fillSolutionsKernel, 1, 1, 1);

		for (int i = 0; i < log2GenerationCount; i++)
		{
			searchCandidateShader.SetInt(ITERATION_NUMBER, i);
			searchCandidateShader.Dispatch(searchCandidateKernel, 1, 1, 1);
		}

		searchCandidateShader.Dispatch(getSolutionKernel, 1,1,1);
		singleSolutionBuffer.GetData(solutions);
		solution = solutions[0];
	}


	private bool ParseInputCoeffs()
	{
		if (!Int32.TryParse(xInputField.text, out a))
		{
			return false;
		}

		if (!Int32.TryParse(yInputField.text, out b))
		{
			return false;
		}

		if (!Int32.TryParse(zInputField.text, out c))
		{
			return false;
		}

		if (!Int32.TryParse(wInputField.text, out d))
		{
			return false;
		}

		if (!Int32.TryParse(fInputField.text, out f))
		{
			return false;
		}

		return true;
	}

	private struct ResolvingVariant
	{
		public float x;
		public float y;
		public float z;
		public float w;
		public float error;

		public ResolvingVariant(float x, float y, float z, float w)
		{
			this.x = x;
			this.y = y;
			this.z = z;
			this.w = w;

			error = Mathf.Abs(a * x + b * y + c * z + d * w - f);
		}


		public override string ToString ()
		{
			return string.Format ("[ResolvingVariant: x={0}, y={1}, z={2}, w={3}, error={4}]", x, y, z, w, error);
		}
		
	}

}
