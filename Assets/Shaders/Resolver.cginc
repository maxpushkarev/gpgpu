﻿#define THREADS_PER_GROUP 64
#define DEFAULT_ABS_VALUE 10
#define MUTATION_POSSIBILITY 0.05
#define MUTABLE_BITS_COUNT 8

struct ResolvingVariant
{
	float x;
	float y;
	float z;
	float w;
	float error;	
};

float A, B, C, D, F;
int iterationNumber;

RWStructuredBuffer<float> accumulationBuffer;
RWStructuredBuffer<float> singleAccumulatedBuffer;

RWStructuredBuffer<ResolvingVariant> solution;
RWStructuredBuffer<ResolvingVariant> singleSolution;

RWStructuredBuffer<ResolvingVariant> generation;

//http://www.reedbeta.com/blog/quick-and-easy-gpu-random-numbers-in-d3d11/
uint rngState;
int seedDeformation;

uint randLCG() {
	rngState = 1664525 * rngState + 1013904223;
	return rngState;
}

uint randXorshift() {
	rngState ^= (rngState << 13);
	rngState ^= (rngState >> 17);
	rngState ^= (rngState << 5);
	return rngState;
}

float rand01(){
	return float(randXorshift()) * 1.0f / 4294967296.0f;
}

int randRange(int min, int max){
	return floor(lerp(min, max, rand01()));
}

ResolvingVariant Interpolate(ResolvingVariant from, ResolvingVariant to, bool coeff){
	ResolvingVariant res;
	res.x = lerp(from.x, to.x, coeff);
	res.y = lerp(from.y, to.y, coeff);
	res.z = lerp(from.z, to.z, coeff);
	res.w = lerp(from.w, to.w, coeff);
	res.error = lerp(from.error, to.error, coeff);
	return res;
}